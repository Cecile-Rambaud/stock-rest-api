<?php

namespace App\Entity;

use App\Repository\InventoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;

/**
 * @ORM\Entity(repositoryClass=InventoryRepository::class)
 * 
 * @ApiResource(
 *     normalizationContext={"groups"={"inventory:read"}},
 *     denormalizationContext={"groups"={"inventory:write"}},
 *     attributes={"order"={"id": "ASC"}}
 * )
 * @ApiFilter(
 *      SearchFilter::class, properties={"state": "exact"}
 * )
* @ApiFilter(
 *      DateFilter::class, properties={
 *          "startDate", "endDate"
 *      }
 * )
 */
class Inventory
{


    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"inventory:read", "inventory_product:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"inventory:read", "inventory:write","inventory_product:read"})
     */
    private $startDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"inventory:read", "inventory:write", "inventory_product:read"})
     */
    private $endDate;

    /**
     * @ORM\Column(type="smallint")
     * @Groups({"inventory:read", "inventory:write","inventory_product:read"})
     */
    private $state;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"inventory:read", "inventory_product:read"})
     */
    private $timeCreated;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"inventory:read", "inventory_product:read"})
     */
    private $timeModified;


    /**
     * @ORM\OneToMany(targetEntity=InventoryProduct::class, mappedBy="inventory", orphanRemoval=true)
     */
    private $inventoryProducts;




    public function __construct()
    {
        $this->timeCreated = new \DateTime();
        $this->inventoryProducts = new ArrayCollection();
    }   

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(?\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getState(): ?int
    {
        return $this->state;
    }

    public function setState(int $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getTimeCreated(): ?\DateTimeInterface
    {
        return $this->timeCreated;
    }

    public function setTimeCreated(\DateTimeInterface $timeCreated): self
    {
        $this->timeCreated = $timeCreated;

        return $this;
    }

    public function getTimeModified(): ?\DateTimeInterface
    {
        return $this->timeModified;
    }

    public function setTimeModified(?\DateTimeInterface $timeModified): self
    {
        $this->timeModified = $timeModified;

        return $this;
    }


    /**
     * @return Collection|InventoryProduct[]
     */
    public function getInventoryProducts(): Collection
    {
        return $this->inventoryProducts;
    }

    public function addInventoryProduct(InventoryProduct $inventoryProduct): self
    {
        if (!$this->inventoryProducts->contains($inventoryProduct)) {
            $this->inventoryProducts[] = $inventoryProduct;
            $inventoryProduct->setInventory($this);
        }

        return $this;
    }

    public function removeInventoryProduct(InventoryProduct $inventoryProduct): self
    {
        if ($this->inventoryProducts->removeElement($inventoryProduct)) {
            // set the owning side to null (unless already changed)
            if ($inventoryProduct->getInventory() === $this) {
                $inventoryProduct->setInventory(null);
            }
        }

        return $this;
    }



}
