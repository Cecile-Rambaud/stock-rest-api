<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\InventoryProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity(repositoryClass=InventoryProductRepository::class)
 * @ORM\Table(name="inventory_product", 
 *    uniqueConstraints={
 *        @UniqueConstraint(
 *            name="inventory_product_unique", 
 *            columns={"product_id", "inventory_id"}
 *        )
 *    }
 * )
 * @ApiResource(
 *     normalizationContext={"groups"={"inventory_product:read"}},
 *     denormalizationContext={"groups"={"inventory_product:write"}},
 *     attributes={"order"={"id": "ASC"}}
 * )
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "product.id": "exact", "inventory.id": "exact","zone.id": "exact"})
 */
class InventoryProduct
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"inventory_product:read", "inventory:read", "zone:read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"inventory_product:write", "inventory_product:read", "inventory:write", "inventory:read", "zone:read"})
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity=Inventory::class, inversedBy="inventoryProducts")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"inventory_product:write", "inventory_product:read", "inventory:write", "inventory:read", "zone:read"})
     */
    private $inventory;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"inventory_product:write", "inventory_product:read", "inventory:write", "inventory:read", "zone:read"})
     */
    private $qty;

    /**
     * @ORM\ManyToOne(targetEntity=Zone::class, inversedBy="inventoryProducts")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"inventory_product:write", "inventory_product:read", "inventory:write", "inventory:read"})
    */
    private $zone;

   

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getInventory(): ?Inventory
    {
        return $this->inventory;
    }

    public function setInventory(?Inventory $inventory): self
    {
        $this->inventory = $inventory;

        return $this;
    }

    public function getQty(): ?int
    {
        return $this->qty;
    }

    public function setQty(int $qty): self
    {
        $this->qty = $qty;

        return $this;
    }

    public function getZone(): ?Zone
    {
        return $this->zone;
    }

    public function setZone(?Zone $zone): self
    {
        $this->zone = $zone;

        return $this;
    }

   
}
