<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\ZoneRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiProperty;

/**
 * @ORM\Entity(repositoryClass=ZoneRepository::class)
 * 
 * @ApiResource(
 *     normalizationContext={"groups"={"zone:read"}},
 *     denormalizationContext={"groups"={"zone:write"}},
 *     attributes={"order"={"id": "ASC"}},
 * )
 */
class Zone
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"zone:read", "inventory_product:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"zone:read", "zone:write", "inventory_product:read"})
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=InventoryProduct::class, mappedBy="zone")
     * @ApiProperty(
     *     readableLink=true
     * )
     * @Groups({"zone:read"})
     */
    private $inventoryProducts;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"zone:read","inventory:read", "inventory_product:read"})
     */
    private $timeCreated;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"zone:read", "inventory_product:read"})
     */
    private $timeModified;



    public function __construct()
    {
        $this->timeCreated = new \DateTime();
        $this->products = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|InventoryProduct[]
     */
    public function getInventoryProducts(): Collection
    {
        return $this->inventoryProducts;
    }

    public function addInventoryProduct(InventoryProduct $inventoryProduct): self
    {
        if (!$this->inventoryProducts->contains($inventoryProduct)) {
            $this->inventoryProducts[] = $inventoryProduct;
            $inventoryProduct->setZone($this);
        }

        return $this;
    }

    public function removeInventoryProduct(InventoryProduct $inventoryProduct): self
    {
        if ($this->inventoryProducts->removeElement($inventoryProduct)) {
            // set the owning side to null (unless already changed)
            if ($inventoryProduct->getZone() === $this) {
                $inventoryProduct->setZone(null);
            }
        }

        return $this;
    }

    public function getTimeCreated(): ?\DateTimeInterface
    {
        return $this->timeCreated;
    }

    public function setTimeCreated(\DateTimeInterface $timeCreated): self
    {
        $this->timeCreated = $timeCreated;

        return $this;
    }

    public function getTimeModified(): ?\DateTimeInterface
    {
        return $this->timeModified;
    }

    public function setTimeModified(?\DateTimeInterface $timeModified): self
    {
        $this->timeModified = $timeModified;

        return $this;
    }
}
