<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CompanyRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity(repositoryClass=CompanyRepository::class)
 * 
  * @ApiResource(
 *     normalizationContext={"groups"={"company:read"}},
 *     denormalizationContext={"groups"={"company:write"}},
 *     attributes={"order"={"id": "ASC"}}
 * )
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "name": "exact"})
 */
class Company
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"company:read", "product:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"company:read", "company:write", "product:write", "product:read"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"company:read", "company:write"})
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"company:read", "company:write"})
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"company:read", "company:write"})
     */
    private $country;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"company:read"})
     */
    private $timeCreated;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"company:read"})
     */
    private $timeModified;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="provider")
     * @Groups({"company:read"})
     */
    private $products;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"company:read", "company:write"})
     */
    private $isSupplier;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->timeCreated = new \DateTime();
    }   

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getTimeCreated(): ?\DateTimeInterface
    {
        return $this->timeCreated;
    }

    public function setTimeCreated(\DateTimeInterface $timeCreated): self
    {
        $this->timeCreated = $timeCreated;

        return $this;
    }

    public function getTimeModified(): ?\DateTimeInterface
    {
        return $this->timeModified;
    }

    public function setTimeModified(?\DateTimeInterface $timeModified): self
    {
        $this->timeModified = $timeModified;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setProvider($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getProvider() === $this) {
                $product->setProvider(null);
            }
        }

        return $this;
    }

    public function getIsSupplier(): ?bool
    {
        return $this->isSupplier;
    }

    public function setIsSupplier(bool $isSupplier): self
    {
        $this->isSupplier = $isSupplier;

        return $this;
    }
}
