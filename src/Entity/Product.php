<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\ProductRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * 
 * @ApiResource(
 *     normalizationContext={"groups"={"product:read"}},
 *     denormalizationContext={"groups"={"product:write"}},
 *     collectionOperations={
 *                "get"={"security"="is_granted('ROLE_API')", "security_message"="Only api user can view product."},
 *                "post"={"security"="is_granted('ROLE_API')", "security_message"="Only api user can add product. "}
 *     } ,
 *     itemOperations={
 *            "get",
 *            "put"={"security"="is_granted('edit', object)"},
 *            "delete"={"security"="is_granted('delete', object)"},
 *            "patch"={"security"="is_granted('edit', object)"}
 *     },
 *     attributes={"order"={"id": "ASC"}}
 * )
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "name": "exact", "barcode": "exact", "brand.name": "exact",  "provider.name": "exact", "availability": "exact"})
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * 
     * @Groups({"product:read", "user:read", "brand:read", "company:read", "zone:read","inventory:read", "inventory_product:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"product:read", "user:read", "product:write", "zone:read","inventory:read", "inventory_product:read"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"product:read", "product:write", "zone:read","inventory:read", "inventory_product:read"})
     */
    private $barcode;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     * @Groups({"product:read", "product:write", "zone:read","inventory:read", "inventory_product:read"})
     */
    private $price;


    /**
     * @ORM\Column(type="integer")
     * @Groups({"product:read", "product:write", "zone:read","inventory:read", "inventory_product:read"})
     */
    private $availability;

    /**
     * @var MediaObject|null
     *
     * @ORM\ManyToOne(targetEntity=MediaObject::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     * @ApiProperty(iri="http://schema.org/image")
     * @Groups({"product:read", "product:write", "inventory:read", "inventory_product:read"})
     */
    private $image = null;

    
    /**
     * @ORM\ManyToOne(targetEntity=Brand::class, inversedBy="products")
     * @Groups({"product:read", "product:write", "inventory_product:read"})
     */
    private $brand;

    /**
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="products")
     * @Groups({"product:read", "product:write", "inventory_product:read"})
     */
    private $provider;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"product:read", "zone:read","inventory:read", "inventory_product:read"})
     */
    private $timeCreated;

    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"product:read", "zone:read","inventory:read", "inventory_product:read"})
     */
    private $timeModified;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="products")
     * @Groups({"product:read", "product:write", "inventory_product:read"})
     */
    private $author;

    
    /**
     * @Groups({"product:read", "product:write", "zone:read","inventory:read", "inventory_product:read"})
     */
    private $qty = 0;


    public function __construct(
        ) {
            $this->timeCreated = new \DateTime();
            $this->zone = new ArrayCollection();
    }    
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBarcode(): ?string
    {
        return $this->barcode;
    }

    public function setBarcode(string $barcode): self
    {
        $this->barcode = $barcode;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getAvailability(): ?int
    {
        return $this->availability;
    }

    public function setAvailability(int $availability): self
    {
        $this->availability = $availability;

        return $this;
    }

    public function getQty(): ?int
    {
        return $this->qty;
    }

    public function setQty(int $qty): self
    {
        $this->qty = $qty;

        return $this;
    }

    public function getImage(): ?MediaObject
    {
        return $this->image;
    }

    public function setImage(?MediaObject $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getTimeCreated(): ?\DateTimeInterface
    {
        return $this->timeCreated;
    }

    public function setTimeCreated(\DateTimeInterface $timeCreated): self
    {
        $this->timeCreated = $timeCreated;

        return $this;
    }

    public function getTimeModified(): ?\DateTimeInterface
    {
        return $this->timeModified;
    }

    public function setTimeModified(?\DateTimeInterface $timeModified): self
    {
        $this->timeModified = $timeModified;

        return $this;
    }

    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    public function setBrand(?Brand $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getProvider(): ?Company
    {
        return $this->provider;
    }

    public function setProvider(?Company $provider): self
    {
        $this->provider = $provider;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }

  

}
