<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BrandRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Entity(repositoryClass=BrandRepository::class)
 * 
 * 
 * @ApiResource(
 *     attributes={"security"= "is_granted('ROLE_API')"},
 *     normalizationContext={"groups"={"brand:read"}},
 *     denormalizationContext={"groups"={"brand:write"}},
 *     attributes={"order"={"id": "ASC"}}
 * )
* @ApiFilter(SearchFilter::class, properties={"id": "exact", "name": "exact"})
 */
class Brand
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @Groups({"brand:read", "product:read"})
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"brand:read", "brand:write", "product:write", "product:read"})
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"brand:read"})
     */
    private $timeCreated;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"brand:read"})
     */
    private $timeModified;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="brand")
     * @Groups({"brand:read"})
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->timeCreated = new \DateTime();
    }   

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTimeCreated(): ?\DateTimeInterface
    {
        return $this->timeCreated;
    }

    public function setTimeCreated(\DateTimeInterface $timeCreated): self
    {
        $this->timeCreated = $timeCreated;

        return $this;
    }

    public function getTimeModified(): ?\DateTimeInterface
    {
        return $this->timeModified;
    }

    public function setTimeModified(?\DateTimeInterface $timeModified): self
    {
        $this->timeModified = $timeModified;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setBrand($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getBrand() === $this) {
                $product->setBrand(null);
            }
        }

        return $this;
    }
}
