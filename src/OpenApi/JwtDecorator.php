<?php
// api/src/OpenApi/JwtDecorator.php

declare(strict_types=1);

namespace App\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\OpenApi;
use ApiPlatform\Core\OpenApi\Model;

final class JwtDecorator implements OpenApiFactoryInterface
{
    private $decorated;

    public function __construct(
        OpenApiFactoryInterface $decorated
    ) {
        $this->decorated = $decorated;
    }

    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);
        $schemas = $openApi->getComponents()->getSchemas();

        $schemas['Token'] = new \ArrayObject([
            'type' => 'object',
            'properties' => [
                'token' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
            ],
        ]);
        $schemas['RefreshToken'] = new \ArrayObject([
            'type' => 'object',
            'properties' => [
                'refresh_token' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
            ],
        ]);

        $schemas['RefreshCredentials'] = new \ArrayObject([
            'type' => 'object',
            'properties' => [
                'refreshToken' => [
                    'type' => 'string',
                    'example' => 'an existing refresh token',
                ]
            ],
        ]);
        
        $schemas['Credentials'] = new \ArrayObject([
            'type' => 'object',
            'properties' => [
                'username' => [
                    'type' => 'string',
                    'example' => 'ausername',
                ],
                'password' => [
                    'type' => 'string',
                    'example' => 'apassword',
                ],
            ],
        ]);

        $pathItem = new Model\PathItem(
            'JWT Token',  // Ref
            null,                // Summary
            null,                // Description
            null,                // Operation GET
            null,                // Operation PUT
            new Model\Operation( // Operation POST
                'postCredentialsItem', // OperationId
                ['Token'],    // Tags
                [                      // Responses
                    '200' => [
                        'description' => 'Get JWT token',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/Token',
                                ],
                            ],
                        ],
                    ],
                ],
                'Return JWT token to login', // Summary
                '',                        // Description
                null,                      // External Docs
                [],                        // Parameters
                new Model\RequestBody(     // RequestBody
                    'Generate new JWT Token',           // Description
                    new \ArrayObject([                   // Content
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/Credentials',
                            ],
                        ],
                    ]),
                    false                               // Required
                ),
            ),
        );
        $openApi->getPaths()->addPath('/authentication_token', $pathItem);



        $pathItem = new Model\PathItem(
            'JWT Refresh Token',  // Ref
            null,                // Summary
            null,                // Description
            null,                // Operation GET
            null,                // Operation PUTTo
            new Model\Operation( // Operation POST
                'postTokenItem', // OperationId
                ['RefreshToken'],    // Tags
                [                      // Responses
                    '200' => [
                        'description' => 'Get JWT refresh token',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/RefreshToken',
                                ],
                            ],
                        ],
                    ],
                ],
                'Return new JWT tokens from refresh token', 
                '',                        
                null,                      
                [],                        
                new Model\RequestBody(     
                    'Generate new JWT tokens', 
                    new \ArrayObject([           
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/RefreshCredentials',
                            ],
                        ],
                    ]),
                    false                        
                ),
            ),
        );
        $openApi->getPaths()->addPath('/token/refresh', $pathItem);
        

        return $openApi;
    }
}