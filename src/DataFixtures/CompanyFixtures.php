<?php

namespace App\DataFixtures;

use App\Entity\Company;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CompanyFixtures extends Fixture
{
    public const REFERENCE_COMPANY_PROVIDER = 'REFERENCE_COMPANY';
    
    public function load(ObjectManager $manager): void
    {
        $company = new Company();
        $company->setName("COGIP");
        $company->setAddress("3 rue de beau soleil 63400 Clermont-Ferrand");
        $company->setPhone("0438483848");
        $company->setCountry("France");
        $company->setIsSupplier(false);
        $manager->persist($company);

        $company = new Company();
        $company->setName("Antalis France");
        $company->setAddress("17 avenue de la Porte des Lilas, Cedex 19, Paris,  75635, France");
        $company->setPhone("0826 080 808");
        $company->setCountry("France");
        $company->setIsSupplier(true);
        $manager->persist($company);
        $this->addReference(self::REFERENCE_COMPANY_PROVIDER, $company);

        $manager->flush();
    }
}
