<?php

namespace App\DataFixtures;

use App\Entity\Inventory;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

use const App\Entity\INVENTORY_STATE_CANCELED;
use const App\Entity\INVENTORY_STATE_CREATED;
use const App\Entity\INVENTORY_STATE_FINISHED;
use const App\Entity\INVENTORY_STATE_STARTED;

class InventoryFixtures extends Fixture  implements DependentFixtureInterface
{

    public const REFERENCE_INVENTORY = 'REFERENCE_INVENTORY_';
    public const INVENTORY_NUMBER = 4;

    public function load(ObjectManager $manager): void
    {
        date_default_timezone_set('Europe/London');
      
        for ($count = 0; $count < self::INVENTORY_NUMBER; $count++) {
            $inventory = new Inventory();
            switch ($count) {
                case 0 :
                    $inventory->setStartDate(new DateTime('2021-12-01 09:00:00'));
                    $inventory->setEndDate(new DateTime('2021-12-01 18:00:00'));
                    $inventory->setState(INVENTORY_STATE_FINISHED);
                    break;
                case 1 :
                    $inventory->setStartDate(new DateTime('2022-01-01 10:00:00'));
                    $inventory->setState(INVENTORY_STATE_CANCELED);
                    break;
                case 2 :
                    $inventory->setStartDate(new DateTime('2022-01-12 09:00:00'));
                    $inventory->setState(INVENTORY_STATE_STARTED);
                    break;
                case 3 :
                    $inventory->setStartDate(new DateTime());
                    $inventory->setState(INVENTORY_STATE_CREATED);
                    break;
            }
            $manager->persist($inventory);
            $this->addReference(self::REFERENCE_INVENTORY.$count, $inventory);
        }


        $manager->flush();
    }
    
    public function getDependencies()
    {
        return [
            ZoneFixtures::class,
        ];
    }
}