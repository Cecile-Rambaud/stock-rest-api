<?php

namespace App\DataFixtures;

use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    public const REFERENCE_PRODUCT = 'REFERENCE_PRODUCT_';
    public const PRODUCT_NUMBER = 20;

    public function load(ObjectManager $manager): void
    {
        $brandNumber = 0;
        for ($count = 0; $count < self::PRODUCT_NUMBER; $count++) {
            $product = new Product();
            $product->setName("Product".$count);
            $product->setQty($count * 2);
            $product->setPrice($count * 5.5);

            $product->setProvider($this->getReference(CompanyFixtures::REFERENCE_COMPANY_PROVIDER));


            if ($brandNumber > count(BrandFixtures::brandNames) - 1) {
                $brandNumber = 0;
            }
            $product->setBrand($this->getReference(BrandFixtures::REFERENCE_BRAND.$brandNumber));
            $brandNumber++;

            if ($count > 9)
                $product->setBarcode("1234567".$count);
            else 
                $product->setBarcode("12345678".$count);
            $product->setAvailability($count % 2);
           
            $manager->persist($product);

            $this->addReference(self::REFERENCE_PRODUCT.$count, $product);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            ZoneFixtures::class,
            BrandFixtures::class,
            CompanyFixtures::class,
            UserFixtures::class
        ];
    }
}
