<?php

namespace App\DataFixtures;

use App\Entity\Zone;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class ZoneFixtures extends Fixture
{
    public const REFERENCE_ZONE = 'REFERENCE_ZONE_';
    public const ZONE_NUMBER = 3;

    public function load(ObjectManager $manager): void
    {
        for ($count = 0; $count < self::ZONE_NUMBER; $count++) {
            $zone = new Zone();
            $zone->setName("Zone".$count);
            $manager->persist($zone);
            $this->addReference(self::REFERENCE_ZONE.$count, $zone);
        }
       
        $manager->flush();
       
    }
}
