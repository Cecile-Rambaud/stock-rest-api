<?php

namespace App\DataFixtures;

use App\Entity\Inventory;
use App\Entity\InventoryProduct;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

use const App\Entity\INVENTORY_STATE_CANCELED;
use const App\Entity\INVENTORY_STATE_CREATED;
use const App\Entity\INVENTORY_STATE_FINISHED;
use const App\Entity\INVENTORY_STATE_STARTED;

class InventoryProductFixtures extends Fixture  implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        date_default_timezone_set('Europe/London');
      
        $nb_products_by_zone = intval(ProductFixtures::PRODUCT_NUMBER /  ZoneFixtures::ZONE_NUMBER);
        $indexZone = 0;
        for (
            $indexProduct = 0; 
            $indexProduct < ProductFixtures::PRODUCT_NUMBER; 
            $indexProduct++
        ) 
        {
            $indexZone = intval($indexProduct / $nb_products_by_zone);
            if ($indexZone >= ZoneFixtures::ZONE_NUMBER) $indexZone = 0;
             
            for ($indexInventory = 0; $indexInventory < InventoryFixtures::INVENTORY_NUMBER; $indexInventory++) 
            {
                $inventoryProduct = new InventoryProduct();
                $inventoryProduct->setQty($indexProduct*2);
                $inventoryProduct->setInventory($this->getReference(InventoryFixtures::REFERENCE_INVENTORY.$indexInventory));
                $inventoryProduct->setProduct($this->getReference(ProductFixtures::REFERENCE_PRODUCT.$indexProduct));
                $inventoryProduct->setZone($this->getReference(ZoneFixtures::REFERENCE_ZONE.$indexZone));
                $manager->persist($inventoryProduct);
            }
        }
        $manager->flush();
    }
    
    public function getDependencies()
    {
        return [
            InventoryFixtures::class,
            ProductFixtures::class,
            ZoneFixtures::class,
        ];
    }
}