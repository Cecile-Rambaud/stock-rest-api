<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    // private UserPasswordHasherInterface $hasher;

    // public function __construct(UserPasswordHasherInterface $hasher)
    // {
    //     $this->hasher = $hasher;
    // }

    
    public function load(ObjectManager $manager): void
    {
        
        $testPassword = "$2y$13\$p0Ff0Ik1dTYhzxckOWW7feas5cN8eI9Whe9ItMMgin9JTxiJ2.HJ2";

        $user = new User();
        $user->setUsername("Audrey");
        $user->setPassword($testPassword);
        $user->setRoles(["ROLE_ADMIN", "ROLE_API", "ROLE_USER"]);
        $user->setEmail("audrey@cogip.com");
        $manager->persist($user);

        $user = new User();
        $user->setUsername("audrey2");
        $user->setPassword($testPassword);
        $user->setRoles(["ROLE_ADMIN", "ROLE_API", "ROLE_USER"]);
        $user->setEmail("audrey2@cogip.com");
        $manager->persist($user);

        $user = new User();
        $user->setUsername("Sam");
        $user->setPassword($testPassword);
        $user->setRoles(["ROLE_API", "ROLE_USER"]);
        $user->setEmail("sam@cogip.com");
        $manager->persist($user);

        $user = new User();
        $user->setUsername("Jean");
        $user->setPassword($testPassword);
        $user->setRoles(["ROLE_USER"]);
        $user->setEmail("jean@cogip.com");
        $manager->persist($user);


        $manager->flush();
    }
}
