<?php

namespace App\DataFixtures;

use App\Entity\Brand;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class BrandFixtures extends Fixture
{
    public const REFERENCE_BRAND = 'REFERENCE_BRAND';
    public const brandNames = ["Canon", "Casio", "Mobilis", "Maped", "Staedtler", "Wonday", "Gautier", "Vinco"];

    public function load(ObjectManager $manager): void
    {
        $brands = [];
        $count = 0;
        
        foreach(self::brandNames as $brandName) {
            $brand = new Brand();
            $brand->setName($brandName);
            $manager->persist($brand);
            $brands[$count] = $brand;
            $count++;
        }
        
        $manager->flush();

        $count--;
        while($count >= 0) {
            $this->addReference(self::REFERENCE_BRAND.$count, $brands[$count]);
            $count--;
        }
    }
}
