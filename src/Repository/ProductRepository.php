<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    
    public function findOneById($id): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.id = :val')
            ->setParameter('val', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    
    public function getProductsNumber(): int
    {
        return $this->createQueryBuilder('p')
            ->select('count(p.id)')
            ->getQuery()
            ->getSingleScalarResult();
        ;
    }
    
    public function getProductQuantity($productId): int
    {
        $queryBuilder = $this->_em->createQueryBuilder();       
        $queryBuilder
            ->select('ip.qty')
            ->from('App\Entity\InventoryProduct', 'ip')
            ->from('App\Entity\Inventory', 'inv')
            ->where('ip.product = :productId')
            ->andWhere('inv.id = ip.inventory')
            ->andWhere('inv.endDate is not NULL')
            ->orderBy('inv.endDate', 'DESC')
            ->setMaxResults(1)
            ->setParameter("productId",$productId);
        $qty = $queryBuilder->getQuery()->getSingleColumnResult();
        if ($qty != null) {
            return $qty[0];
        }
        return 0;
    }
}