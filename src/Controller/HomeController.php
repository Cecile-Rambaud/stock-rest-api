<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(ProductRepository $productRepository): Response
    {
        $numberProducts = $productRepository->getProductsNumber();
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'version' => '1.0',
            'url_react_admin_app' => $this->getParameter('app.url_react_admin_app'),
            'number_products' => $numberProducts
        ]);
    }
}
