<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AndroidAppController extends AbstractController
{
    /**
     * @Route("/android_app", name="android_app")
     */
    public function index(): Response
    {
        return $this->render('android_app/index.html.twig', [
   
        ]);
    }
}
