<?php
namespace App\Tests;

use App\Entity\User;

final class UsersTest extends AbstractTest
{
    public function testGetCollectionAuthorized(): void
    {
        // test authorized
        $this->createClientWithCredentials()->request('GET', '/api/users');
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }

    public function testGetCollectionUnAuthorized(): void
    {
        // test not authorized
        $this->createClientWithDefaultOptions()->request('GET', '/api/users');
        $this->assertResponseStatusCodeSame(401);
    }

 
    public function testAddFindDeleteUser(): void
    {
        
        //CREATE USER
        $this->createClientWithCredentials()->request('POST', '/api/users', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'username' => 'ABCD',
                'password' => 'test',
                'roles'=> ["ROLE_ADMIN", "ROLE_API", "ROLE_USER"],
                'email'=> 'ABCD@COGIP.COM'
             ]
            ]);
        $this->assertResponseStatusCodeSame(201);

        //FIND CREATED USER
        $iri = $this->findIriBy(User::class, ['username' => 'ABCD']);
        $response = $this->createClientWithCredentials()->request('GET', $iri);
        $this->assertResponseIsSuccessful();

         //DELETE CREATED USER
         $this->createClientWithCredentials()->request('DELETE', $iri);
    }
 

    protected function getFixtures() : iterable {
        return [];
    }
}