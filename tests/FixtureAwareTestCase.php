<?php
namespace App\Tests;


use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bridge\Doctrine\DataFixtures\ContainerAwareLoader;

abstract class FixtureAwareTestCase  extends ApiTestCase {

    
    /**
     * @var ContainerAwareLoader
     */
    private $fixtureLoader;
    /**
     * @var ORMExecutor
     */
    private $fixtureExecutor;

    
    public function setUp(): void
    {
        self::bootKernel();
    }

    protected function addFixture(FixtureInterface $fixture) {
        $this->getFixtureLoader()->addFixture($fixture);
    }

    protected function executeFixtures() {
        $this->getFixtureExecutor()->execute($this->getFixtureLoader()->getFixtures());
    }

    private function getFixtureExecutor() {
        if (!$this->fixtureExecutor) {
            $entityManager = self::$kernel->getContainer()->get('doctrine')->getManager();
            $this->fixtureExecutor = new ORMExecutor($entityManager, new ORMPurger($entityManager));
        }
        return  $this->fixtureExecutor;
    }

    private function getFixtureLoader() {
        if (!$this->fixtureLoader) {
            $this->fixtureLoader = new ContainerAwareLoader(self::$kernel->getContainer());
        }
        return  $this->fixtureLoader; 
    }

}