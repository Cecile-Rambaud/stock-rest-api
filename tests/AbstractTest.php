<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use App\DataFixtures\UserFixtures;
use App\Tests\FixtureAwareTestCase;

abstract class AbstractTest extends FixtureAwareTestCase
{
    /** @var AbstractDatabaseTool */
    protected $databaseTool;

    private $token;
    

    protected $api_options = [
        'auth_basic' => null,
        'auth_bearer' => null,
        'query' => [],
        //'headers' => ['accept' => ['application/ld+json']],
        'headers' => ['accept' => '*/*', 'Content-Type' => 'application/json'],
        'body' => '',
        'base_uri' => 'http://localhost/api',
        'extra' => [],
    ];
    
    public function setUp(): void
    {
        self::bootKernel();
        $this->addFixture(new UserFixtures());  
        $fixtures = $this->getFixtures();
        foreach ($fixtures as $fixture) {
            $this->addFixture($fixture);   
        }
        $this->executeFixtures();
    }

    protected function createClientWithDefaultOptions(): Client
    {
        $client = static::createClient();
        $client->setDefaultOptions($this->api_options);
        return $client;
    }

    protected function createClientWithCredentials(): Client
    {
        $token = $this->getToken();
        $options = $this->api_options;
        $options['headers'] =  ['accept' => ['*/*'], 'Content-Type' => 'application/json', 'authorization' => 'Bearer '.$token];
        $client = static::createClient($options, $options);
        return $client;
    }

    protected function apiOptionWithCredentials(): array
    {
        $token = $this->getToken();
        $options = $this->api_options;
        $options['headers'] =  ['accept' => ['*/*'], 'Content-Type' => 'application/json', 'authorization' => 'Bearer '.$token];
        return $options;
    }

    /**
     * Use other credentials if needed.
     */
    protected function getToken(): string
    {
       
        if ($this->token) {
            return $this->token;
        }
        $client = $this->createClientWithDefaultOptions();
        $response = $client->request('POST', '/authentication_token', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'username' => 'audrey2',
                'password' => 'test',
            ],
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('token',  $response->toArray());

        $data = json_decode($response->getContent());
        
        $this->token = $data->token;
        
        return $this->token;
    }

    abstract protected function getFixtures() : iterable;
}