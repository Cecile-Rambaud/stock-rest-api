<?php

namespace App\Tests;

use App\DataFixtures\ZoneFixtures;
use App\Entity\Product;
use App\Entity\Zone;

class ProductTest extends AbstractTest
{

    public function testGetCollectionAuthorized(): void
    {
        // test authorized
        $this->createClientWithCredentials()->request('GET', '/api/products');
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
    }

    public function testGetCollectionUnAuthorized(): void
    {
        // test not authorized
        $this->createClientWithDefaultOptions()->request('GET', '/api/products');
        $this->assertResponseStatusCodeSame(401);
    }

 
    public function testAddFindDeleteProduct(): void
    {
        $zone = new Zone();

        $iri = $this->findIriBy(Zone::class, ['name' => 'ZoneA']);
        $response = $this->createClientWithCredentials()->request('GET', $iri);
        $this->assertResponseIsSuccessful();
        $data = json_decode($response->getContent());
        $zoneAId = $data->{'@id'};
        
        //CREATE PRODUCT
        $response = $this->createClientWithCredentials()->request('POST', '/api/products', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
            'name' => 'imprimante',
           'barcode'=> '12345689',
           'price'=> '119.99',
           'qty'=> 1,
           'availability' => 1,
           'zone' => $zoneAId
        ]]);
        $this->assertResponseStatusCodeSame(201);

        //FIND CREATED PRODUCT
        $iri = $this->findIriBy(Product::class, ['name' => 'imprimante']);
        $response = $this->createClientWithCredentials()->request('GET', $iri);
        $this->assertResponseIsSuccessful();

         //DELETE CREATED PRODUCT
         $this->createClientWithCredentials()->request('DELETE', $iri);
    }

    protected function getFixtures() : iterable {
        return [new ZoneFixtures()];
    }
}