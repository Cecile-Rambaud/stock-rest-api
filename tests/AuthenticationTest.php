<?php

namespace App\Tests;

class AuthenticationTest extends AbstractTest
{
    public function testGetToken(): void
    {
        $client = $this->createClientWithDefaultOptions();
        // retrieve a token
        $response = $client->request('POST', '/authentication_token', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'username' => 'audrey2',
                'password' => 'test',
            ],
        ]);

        $json = $response->toArray();
        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('token', $json);
    }


    protected function getFixtures() : iterable {
        return [];
    }
}