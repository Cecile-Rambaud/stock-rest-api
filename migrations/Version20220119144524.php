<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220119144524 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE inventory_product (id INT AUTO_INCREMENT NOT NULL, product_id INT NOT NULL, inventory_id INT NOT NULL, zone_id INT NOT NULL, qty INT NOT NULL, INDEX IDX_924EA2514584665A (product_id), INDEX IDX_924EA2519EEA759 (inventory_id), INDEX IDX_924EA2519F2C3FAB (zone_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE inventory_product ADD CONSTRAINT FK_924EA2514584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE inventory_product ADD CONSTRAINT FK_924EA2519EEA759 FOREIGN KEY (inventory_id) REFERENCES inventory (id)');
        $this->addSql('ALTER TABLE inventory_product ADD CONSTRAINT FK_924EA2519F2C3FAB FOREIGN KEY (zone_id) REFERENCES zone (id)');
        $this->addSql('DROP TABLE inventory_zone');
        $this->addSql('ALTER TABLE product CHANGE time_created time_created DATETIME NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE inventory_zone (inventory_id INT NOT NULL, zone_id INT NOT NULL, INDEX IDX_EA4D0F299EEA759 (inventory_id), INDEX IDX_EA4D0F299F2C3FAB (zone_id), PRIMARY KEY(inventory_id, zone_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE inventory_zone ADD CONSTRAINT FK_EA4D0F299EEA759 FOREIGN KEY (inventory_id) REFERENCES inventory (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('ALTER TABLE inventory_zone ADD CONSTRAINT FK_EA4D0F299F2C3FAB FOREIGN KEY (zone_id) REFERENCES zone (id) ON UPDATE NO ACTION ON DELETE CASCADE');
        $this->addSql('DROP TABLE inventory_product');
        $this->addSql('ALTER TABLE product CHANGE time_created time_created DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
    }
}
