const https = require('https');
const fs = require('fs');
const express = require('express');
var cors = require('cors');
const path = require('path');
const app = express();

var port = 3000;

var options = {
    key: fs.readFileSync('/etc/apache2/ssl/app.key'),
    cert: fs.readFileSync('/etc/apache2/ssl/cert_chained.crt'),
};

var project_root = __dirname;

app.use(express.static(path.join(project_root, 'build')));
console.log("express app loaded");
app.use(cors());
console.log("cors loaded into app");
app.get('/', function (req, res) {
  res.sendFile(path.join(project_root, 'build', 'index.html'));
});
console.log("get / configured");

const httpsServer = https.createServer(options, app);

httpsServer.listen(port, function () { console.log("Server listening") });
