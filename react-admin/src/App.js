// admin/src/App.js
import React from "react";
import { Redirect, Route } from "react-router-dom";
import { 
  hydraDataProvider as baseHydraDataProvider, 
  fetchHydra as baseFetchHydra,
  useIntrospection,
  FieldGuesser,
  ListGuesser,
  ResourceGuesser,
  CreateGuesser,
  EditGuesser,
  InputGuesser
} from "@api-platform/admin";
import { ReferenceField, ReferenceArrayField, SingleFieldList, ChipField, TextField, 
  SimpleFormIterator, PasswordInput, 
  ArrayInput, ReferenceInput, TextInput, ReferenceArrayInput, ReferenceManyField, 
  AutocompleteInput, AutocompleteArrayInput, SelectInput, 
  FileField, FileInput, ImageField , ImageInput, required   } from "react-admin";
import parseHydraDocumentation from "@api-platform/api-doc-parser/lib/hydra/parseHydraDocumentation";
import authProvider from "./authProvider";
import ShowGuesser from "@api-platform/admin/lib/ShowGuesser";
import constants from "./Constants"

const getHeaders = () => {
  return {
     // 'Content-Type': 'application/json;charset=UTF-8',
     // "Access-Control-Allow-Origin": constants.URL,
      "Authorization": (window.localStorage.getItem("token"))?`Bearer ${window.localStorage.getItem("token")}`:"",
  };
}

const fetchHydra = (url,  options = {credentials : 'include'}) =>
  baseFetchHydra(
    url, {
      ...options,
      user : authProvider.getUser(),
      options: {credentials : 'include'}
      //,headers: getHeaders,
    }
  );


const RedirectToLogin = () => {
  const introspect = useIntrospection();

  if (window.localStorage.getItem("token")) {
    introspect();
    return <></>;
  }
  return <Redirect to="/login" />; //return <Redirect to="/authent" />;
};

const apiDocumentationParser = async () => {
    try {
      const { api } = await parseHydraDocumentation(constants['ENTRYPOINT'], { headers: getHeaders });
      return { api };
    } catch (result) {
      if (result.status === 401) {
        // Prevent infinite loop if the token is expired
        window.localStorage.removeItem("token");
  
        return {
          api: result.api,
          customRoutes: [
            <Route key="/" path="/"  component={RedirectToLogin} />
          ],
        };
      }
      throw result;
    }
  };

const MediaObjectsCreate = props => (
    <CreateGuesser {...props}>
      <FileInput source="file">
        <FileField source="src" title="title" />
      </FileInput>
    </CreateGuesser>
  );
  

const dataProvider = baseHydraDataProvider({
  entrypoint: constants['ENTRYPOINT'],
  httpClient: fetchHydra,
  apiDocumentationParser,
  useEmbedded: true});

  const UsersList = (props) => (
    <ListGuesser {...props}>
      <FieldGuesser source="username" />
      <FieldGuesser source="email" />
      <FieldGuesser source="roles" />
      {/* Use react-admin components directly when you want complex fields. */}
      {/*
      <ReferenceField label="Product name" source="product" reference="products">
        <TextField source="name" />
      </ReferenceField>
      */}
    </ListGuesser>
  );

  const UsersEdit = (props) => (
    <EditGuesser {...props}>
      <InputGuesser source="username" />
      <PasswordInput source="password" />
      <InputGuesser source="email" />
      <ArrayInput source="roles">
        <SimpleFormIterator>
        <TextInput />
        </SimpleFormIterator>
      </ArrayInput>
    </EditGuesser>
  )

  
  const BrandsList = (props) => (
    <ListGuesser {...props}>
      <FieldGuesser source="name" />
    </ListGuesser>
  );

  const CompaniesList = (props) => (
    <ListGuesser {...props}>
      <FieldGuesser source="name" />
      <FieldGuesser source="email" />
      <FieldGuesser source="address" />
      <FieldGuesser source="country" />
      <FieldGuesser source="isSupplier" />
    </ListGuesser>
  );

  const InventoriesList = (props) => (
    <ListGuesser {...props}>
      <FieldGuesser source="startDate" />
      <FieldGuesser source="endDate" />
      <FieldGuesser source="state" />
      {/*
      <ReferenceArrayField label="Zones" source="zone" reference="zones">
          <SingleFieldList>
            <ChipField source="name" />
          </SingleFieldList>
      </ReferenceArrayField>
      */}
    </ListGuesser>
  );




  const ProductsList = (props) => (
    <ListGuesser {...props}>
      <FieldGuesser source="name" />
      <FieldGuesser source="barcode" />
      <FieldGuesser source="price" />
      <FieldGuesser source="qty" />
      <FieldGuesser source="availability" />

    </ListGuesser>
  );

  
  const ZonesList = (props) => (
    <ListGuesser {...props}>
      <FieldGuesser source="name" />
    </ListGuesser>
  );


  const InventoryCreate = props => (
    <CreateGuesser {...props}>
      <InputGuesser source="startDate" />
      <InputGuesser source="endDate" />
      <InputGuesser source="state" />
      <ReferenceInput
        source="zone"
        reference="zones"
        label="Zones"
        filterToQuery={name => ({ name: name })}
      >
        <AutocompleteInput optionText="name" />
      </ReferenceInput>

    </CreateGuesser>
  );

  const ProductEdit = props => (
    <EditGuesser {...props}>
      <InputGuesser source="name" />
      <InputGuesser source="barcode" />
      <InputGuesser source="price" />
      <InputGuesser source="qty" />
      <InputGuesser source="availability" />
    
      <ReferenceInput label="Brand" source="brand" reference="brands" validate={[required()]}>
                <SelectInput optionText="name" />
      </ReferenceInput>

      <ReferenceInput label="Provider" source="company" reference="companies" validate={[required()]}>
                <SelectInput optionText="name" />
      </ReferenceInput>
      
      <ImageInput accept="image/*" multiple="false" source="image">
                            <ImageField source="contentUrl"/>
      </ImageInput>
    </EditGuesser>
  );
  
  const InventoryEdit = props => (
    <EditGuesser {...props}>
      <InputGuesser source="startDate" />
      <InputGuesser source="endDate" />
      <InputGuesser source="state" />
  
      <ReferenceArrayInput
        source="zone"
        reference="zones"
        label="Zones"
        filterToQuery={name => ({ title: name })}
      >
        <AutocompleteArrayInput optionText="name" />
      </ReferenceArrayInput>

    </EditGuesser>
  );

  const InventoryShow = props => (
    <ShowGuesser {...props}>
      <FieldGuesser source="startDate" addLabel={true}/>
      <FieldGuesser source="endDate" addLabel={true}/>
      <FieldGuesser source="state" addLabel={true}/>

      <ReferenceManyField reference="zones" target="zone" label="Zones">
        <SingleFieldList>
            <ChipField source="name" />
        </SingleFieldList>
    </ReferenceManyField>
      

    

    </ShowGuesser>
  );


  
const AdminLoader = () => {
  
    if (typeof window !== "undefined") {
      const { HydraAdmin } = require("@api-platform/admin");
      return <HydraAdmin 
        dataProvider={dataProvider} 
        authProvider={authProvider} 
        entrypoint={window.origin}>
          
          <ResourceGuesser name="users" 
            list={UsersList}
            edit={UsersEdit}
           />



          <ResourceGuesser name="products" 
              list={ProductsList} 
              edit={ProductEdit} 
              />

          <ResourceGuesser 
            name="inventories" 
            list={InventoriesList}
            create={InventoryCreate}
            edit={InventoryEdit} 
            show={InventoryShow}  />
          
          <ResourceGuesser name="brands" list={BrandsList} />
          <ResourceGuesser name="companies" list={CompaniesList} />
          <ResourceGuesser name="zones" list={ZonesList} />

          <ResourceGuesser name="media_objects" create={MediaObjectsCreate} />
        </HydraAdmin>;
    }
  
    return <></>;
};

const Admin = () => (
    
    <AdminLoader />

);
export default Admin;