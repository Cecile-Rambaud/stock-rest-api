const constants = {
    URL : process.env.REACT_APP_API_URL,
    ENTRYPOINT : process.env.REACT_APP_API_ENTRYPOINT,
    AUTH_PATH : process.env.REACT_APP_API_AUTH_PATH
}

export default constants;