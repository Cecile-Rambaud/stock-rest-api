import decodeJwt from 'jwt-decode';
import constants from "./Constants"


function hasDateExpired(aDate) {
  return Date.now() > aDate * 1000;
}

// in src/authProvider.js
const authProvider = {
  login: ({ username, password }) =>  {
    const request = new Request(constants["AUTH_PATH"], {
      method: 'POST',
      body: JSON.stringify({ username, password }),
      headers: new Headers({ 'Content-Type': 'application/json' }),
    });
    return fetch(request)
      .then(response => {
        if (response.status < 200 || response.status >= 300) {
          throw new Error(response.statusText);
        }
        return response.json();
      })
      .then(({ token }) => {
        window.localStorage.setItem('token', token);
      });
  },
  logout: () => {
    window.localStorage.removeItem('token');
    return Promise.resolve();
  },
  checkError: (error) => {
    const status = error.status;
    if (status === 401 || status === 403) {
      window.localStorage.removeItem('token');
      return Promise.reject();
    }
    return Promise.resolve();
  },
  checkAuth: () => {
    return window.localStorage.getItem('token') ? Promise.resolve() : Promise.reject();
  },
  getUser: () => {
    const token = window.localStorage.getItem('token');
    const decodedToken = decodeJwt(token);
    return {"authenticated":!hasDateExpired(decodedToken.exp), "token": `Bearer ${token}`};
  },
  getPermissions: () => {
      const token = window.localStorage.getItem('token');
      const decodedToken = decodeJwt(token);
      if (!decodedToken) {
        window.localStorage.removeItem('token');
        return Promise.reject();
      }
      const isTokenExpired = hasDateExpired(decodedToken.exp);
      if (isTokenExpired) {
        window.localStorage.removeItem('token');
        return Promise.reject();
      }
      const role = decodedToken.roles;
      return role ? Promise.resolve(role) : Promise.reject();
   }
};

export default authProvider;
